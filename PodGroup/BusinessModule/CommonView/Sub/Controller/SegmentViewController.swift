//
//  SegmentViewController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/22.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
import XFYSegmentView

class SegmentViewController: BaseViewController {

    @IBOutlet weak var segmentView1: SegmentView!
    @IBOutlet weak var segmentView2: SegmentView!
    @IBOutlet weak var segmentView3: SegmentView!
    @IBOutlet weak var segmentView4: SegmentView!
    
    @IBOutlet weak var segmentGroupView: SegmentGroupView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func randomSegmentView(_ sender: Any) {
        randomCreateSegment()
    }
}

// MARK: - 私有
private extension SegmentViewController {
    
    func initView() {
        segmentView1.setupStyle(itemMode: .divide, lineMode: .full)
        segmentView2.setupStyle(itemMode: .divide, lineMode: .fixed)
        segmentView3.setupStyle(itemMode: .arrange, lineMode: .full)
        segmentView4.setupStyle(itemMode: .arrange, lineMode: .fixed)
        randomCreateSegment()
        
        let view1 = UIView()
        view1.backgroundColor = .red
        let view2 = UIView()
        view2.backgroundColor = .orange
        let view3 = UIView()
        view3.backgroundColor = .gray
        let view4 = UIView()
        view4.backgroundColor = .blue
        let view5 = UIView()
        view5.backgroundColor = .yellow
        segmentGroupView.items = [
            ("选项1", view1),
            ("第二选项", view2),
            ("选项三哦哦哦哦哦哦", view3),
            ("四", view4),
            ("555555", view5)
        ]
        segmentGroupView.setupStyle(font: .systemFont(ofSize: 12), selectedColor: .orange, itemMode: .arrange, lineMode: .fixed, segmentViewHeight: 30)
        segmentGroupView.selectIndexChange = { index in
            print("第\(index)选项")
        }
        
        let segmentView = SegmentView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 80, width: 300, height: 40))
        segmentView.setItem(titles: ["代码", "创建", "的", "SegmentView"])
        segmentView.setupStyle(font: .boldSystemFont(ofSize: 12), normalColor: .gray, selectedColor: .orange, itemMode: .arrange, lineMode: .full)
        segmentView.didSelectAtIndex = { index in
            print("点击了\(index)")
        }
        view.addSubview(segmentView)
    }
    
    func randomCreateSegment() {
        var titles: [String] = []
        let count = Int(arc4random()%10)
        print("随机数为：", count)
        (0...count).forEach { i in
            titles.append("第\(i)选项")
        }
        segmentView1.setItem(titles: titles)
        segmentView2.setItem(titles: titles)
        segmentView3.setItem(titles: titles)
        segmentView4.setItem(titles: titles)
    }
}
