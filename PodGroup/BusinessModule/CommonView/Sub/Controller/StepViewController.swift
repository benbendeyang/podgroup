//
//  StepViewController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/4/17.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
import XFYStepView

class StepViewController: BaseViewController {

    @IBOutlet weak var stepView: StepView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    

    // MARK: 操作
    @IBAction func lastStep(_ sender: Any) {
        stepView.currentProgress = max(0, stepView.currentProgress - 1)
    }
    @IBAction func nextStep(_ sender: Any) {
        stepView.currentProgress = min(stepView.titles.count - 1, stepView.currentProgress + 1)
    }
    @IBAction func randomStepView(_ sender: Any) {
        randomCreate()
    }
}

// MARK: - 私有
private extension StepViewController {
    
    func initView() {
        randomCreate()
    }
    
    func randomCreate() {
        let count = Int(arc4random()%6) + 1
        var titles = [String]()
        for i in 0..<count {
            titles.append("第\(i + 1)项")
        }
        stepView.titles = titles
        stepView.currentProgress = Int(arc4random()%UInt32(count))
        print("创建\(count)项,当前第\(stepView.currentProgress + 1)")
    }
}
