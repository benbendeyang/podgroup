//
//  AssemblyMainController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/16.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit

class AssemblyMainController:  BaseViewController {
        
    @IBOutlet weak var mainTableView: UITableView!
    private var items: [(title: String, describe: String, segue: String)] = [
        ("弹窗", "XFYPopView", "PopController"),
        ("图片浏览", "XFYImageBrowser", "BrowserImageController"),
        ("WebView", "XFYWebView", "UseWebViewController"),
        ("媒体选择", "XFYSelectMedia", "SelectMediaController"),
        ("动画", "XFYAnimation", "AnimationController")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}

// MARK: 列表
extension AssemblyMainController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "ItemCell")
        let item = items[indexPath.row]
        cell.textLabel?.text = item.title
        cell.detailTextLabel?.text = item.describe
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        performSegue(withIdentifier: item.segue, sender: nil)
    }
}
