//
//  TipView.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/19.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
import XFYPopView

class TipView: PopContentView {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
    
    @IBAction func close(_ sender: Any) {
        delegate?.popViewDismiss()
    }
}
