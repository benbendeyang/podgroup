//
//  BrowserImageController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/20.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
import XFYImageBrowser
import XFYCycleScrollView

class BrowserImageController: BaseViewController {

    @IBOutlet weak var bannerView: BannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func browser(_ sender: Any) {
        let imageUrls: [String] = [
            "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg",
            "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg",
            "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg"
        ]
        presentImageBrowser(imageUrls: imageUrls)
    }
}

private extension BrowserImageController {
    
    func initView() {
        let imageUrls: [String] = [
            "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg",
            "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg",
            "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg"
        ]
        bannerView.setupBanner(urls: imageUrls, placeholder: #imageLiteral(resourceName: "default"))
        bannerView.didTouchAtIndex = { [weak self] index in
            self?.presentImageBrowser(imageUrls: imageUrls, placeholder: #imageLiteral(resourceName: "default"), selectIndex: index)
        }
    }
}
