//
//  UseWebViewController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/21.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
import XFYWebView

class UseWebViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: 操作
    @IBAction func openBaidu(_ sender: Any) {
        let webViewController = WebViewController(url: "https://www.baidu.com")
        navigationController?.pushViewController(webViewController, animated: true)
    }
}
