//
//  PopController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/18.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
import XFYPopView

class PopController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectPopView(_ sender: UIButton) {
        PopSelectManager.shared.popSelectView(at: sender, titles: ["选项1", "选项二", "长长的选项"], selectIndex: 0) { index in
            print("点击:", index)
        }
    }
    @IBAction func windowPop(_ sender: UIButton) {
        guard let tipView = Bundle.main.loadNibNamed("TipView", owner: nil, options: nil)?.last as? TipView else {
            return
        }
        tipView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: tipView)
    }
    @IBAction func controllerPop(_ sender: UIButton) {
        guard let tipView = Bundle.main.loadNibNamed("TipView", owner: nil, options: nil)?.last as? TipView else {
            return
        }
        tipView.titleLabel.text = sender.currentTitle
        PopViewController.pop(on: self, contentView: tipView)
    }
}
