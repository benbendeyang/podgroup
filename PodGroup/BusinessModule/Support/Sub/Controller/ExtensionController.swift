//
//  ExtensionController.swift
//  PodGroup
//
//  Created by 🐑 on 2019/3/28.
//  Copyright © 2019 Yang. All rights reserved.
//

import UIKit
@_exported import XFYExtension

class ExtensionController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func clickButton(_ sender: Any) {
        button.countDown(count: 12, normal: "我是一个倒计时按钮", decrease: "剩余/s")
    }
    @IBAction func toast(_ sender: Any) {
        view.toast("去看XFYExtension吧～")
    }
}

// MARK: - 私有
private extension ExtensionController {
    
    func initView() {
        imageView.setImage(url: "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg", placeholder: #imageLiteral(resourceName: "default"))
        label.text =
        """
        偷懒一波，具体有哪些扩展请看XFYExtension
        """
    }
}
